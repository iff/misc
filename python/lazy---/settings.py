from os.path import join, dirname
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_settingsWindow(object):
	def __init__(self, dirname):
		self.dirname = dirname

	def makeSavable(self):
		self.interactLine.setText(str(self.interactLine.text()).upper())
		self.inventoryLine.setText(str(self.inventoryLine.text()).upper())
		self.saveButton.setEnabled(True)

	def save(self):
		global k, l
		k = str(self.interactLine.text()).lower()
		l = str(self.inventoryLine.text()).lower()
		data = open(join(self.dirname, "data/config.dat"), "w")
		data.write(k + l)
		data.close()
		self.saveButton.setEnabled(False)

	def loadConfig(self):
		global k, l
		data = open(join(self.dirname, "data/config.dat"), "r")
		line = data.readline()
		k = line[0]
		l = line[1]
		data.close()
		self.interactLine.setText(str(k).upper())
		self.inventoryLine.setText(str(l).upper())
		self.saveButton.setEnabled(False)

	def setupUi(self, settingsWindow):
		settingsWindow.setObjectName("settingsWindow")
		settingsWindow.resize(201, 115)
		settingsWindow.setMinimumSize(QtCore.QSize(201, 115))
		settingsWindow.setMaximumSize(QtCore.QSize(201, 115))
		font = QtGui.QFont()
		font.setFamily("Microsoft YaHei")
		font.setPointSize(12)
		settingsWindow.setFont(font)
		self.centralwidget = QtWidgets.QWidget(settingsWindow)
		self.centralwidget.setObjectName("centralwidget")
		self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
		self.verticalLayout.setObjectName("verticalLayout")
		self.formLayout = QtWidgets.QFormLayout()
		self.formLayout.setObjectName("formLayout")
		self.interactLabel = QtWidgets.QLabel(self.centralwidget)
		self.interactLabel.setObjectName("interactLabel")
		self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.interactLabel)
		self.inventoryLabel = QtWidgets.QLabel(self.centralwidget)
		self.inventoryLabel.setObjectName("inventoryLabel")
		self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.inventoryLabel)
		self.interactLine = QtWidgets.QLineEdit(self.centralwidget)
		self.interactLine.setAlignment(QtCore.Qt.AlignCenter)
		self.interactLine.setMaxLength(1)
		self.interactLine.textEdited.connect(self.makeSavable)
		self.interactLine.setObjectName("interactLine")
		self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.interactLine)
		self.inventoryLine = QtWidgets.QLineEdit(self.centralwidget)
		self.inventoryLine.setAlignment(QtCore.Qt.AlignCenter)
		self.inventoryLine.setMaxLength(1)
		self.interactLine.textEdited.connect(self.makeSavable)
		self.inventoryLine.textEdited.connect(self.makeSavable)
		self.inventoryLine.setObjectName("inventoryLine")
		self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.inventoryLine)
		self.verticalLayout.addLayout(self.formLayout)
		self.saveButton = QtWidgets.QPushButton(self.centralwidget)
		self.saveButton.setObjectName("saveButton")
		self.saveButton.clicked.connect(self.save)
		self.verticalLayout.addWidget(self.saveButton)
		settingsWindow.setCentralWidget(self.centralwidget)
		self.loadConfig()

		self.retranslateUi(settingsWindow)
		QtCore.QMetaObject.connectSlotsByName(settingsWindow)

	def retranslateUi(self, settingsWindow):
		_translate = QtCore.QCoreApplication.translate
		settingsWindow.setWindowTitle(_translate("settingsWindow", "---"))
		self.interactLabel.setText(_translate("settingsWindow", "---"))
		self.inventoryLabel.setText(_translate("settingsWindow", "---"))
		self.saveButton.setText(_translate("settingsWindow", "---"))
